package de.neoskop.magnolia.xliff.app.exporter;

import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.framework.app.BaseSubApp;
import javax.inject.Inject;

public class XliffExporterSubApp extends BaseSubApp<XliffExporterView> {

  @Inject
  public XliffExporterSubApp(SubAppContext subAppContext, XliffExporterPresenter presenter) {
    super(subAppContext, presenter.start());
  }

  public XliffExporterView start(Location location) {
    return super.start(location);
  }

  @Override
  public String getCaption() {
    return "Export";
  }
}
