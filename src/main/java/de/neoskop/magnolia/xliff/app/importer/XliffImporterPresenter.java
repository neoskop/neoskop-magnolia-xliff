package de.neoskop.magnolia.xliff.app.importer;

import de.neoskop.magnolia.xliff.service.XliffImporterService;
import info.magnolia.ui.api.context.UiContext;
import java.io.File;
import javax.inject.Inject;

/**
 * @author Arne Diekmann
 * @since 08.05.17
 */
public class XliffImporterPresenter implements XliffImporterView.Listener {

  private final XliffImporterService xliffImporterService;
  private final UiContext context;
  private XliffImporterView view;

  @Inject
  public XliffImporterPresenter(
      XliffImporterService xliffImporterService, UiContext context, XliffImporterView view) {
    this.xliffImporterService = xliffImporterService;
    this.context = context;
    this.view = view;
  }

  XliffImporterView start() {
    view.setListener(this);
    return view;
  }

  @Override
  public void importXliff(File file) {
    xliffImporterService.importFile(file);
  }
}
