package de.neoskop.magnolia.xliff.app.importer;

import info.magnolia.ui.api.app.SubAppContext;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.framework.app.BaseSubApp;
import javax.inject.Inject;

/**
 * @author Arne Diekmann
 * @since 08.05.17
 */
public class XliffImporterSubApp extends BaseSubApp<XliffImporterView> {

  @Inject
  public XliffImporterSubApp(SubAppContext subAppContext, XliffImporterPresenter presenter) {
    super(subAppContext, presenter.start());
  }

  public XliffImporterView start(Location location) {
    return super.start(location);
  }

  @Override
  public String getCaption() {
    return "Import";
  }
}
