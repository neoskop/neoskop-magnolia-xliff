package de.neoskop.magnolia.xliff.app.importer;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.v7.ui.HorizontalLayout;
import com.vaadin.v7.ui.Upload;
import info.magnolia.cms.core.Path;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.form.field.upload.UploadReceiver;
import info.magnolia.ui.vaadin.layout.SmallAppLayout;
import java.io.File;
import javax.inject.Inject;

/**
 * @author Arne Diekmann
 * @since 08.05.17
 */
public class XliffImporterViewImpl implements XliffImporterView {

  private final SmallAppLayout root = new SmallAppLayout();
  private final UiContext uiContext;
  private final SimpleTranslator translator;
  private Listener listener;
  private Upload uploadField;
  private UploadReceiver uploadReceiver;

  @Inject
  public XliffImporterViewImpl(UiContext uiContext, SimpleTranslator translator) {
    this.uiContext = uiContext;
    this.translator = translator;
  }

  @Override
  public void setListener(Listener listener) {
    this.listener = listener;
  }

  @Override
  public Component asVaadinComponent() {
    Button exportButton =
        new Button(
            "Importieren",
            (Button.ClickListener)
                event -> {
                  final File file = uploadReceiver.getFile();

                  if (file != null) {
                    listener.importXliff(file);
                  }
                });
    exportButton.addStyleName("v-button-smallapp");
    exportButton.addStyleName("commit");
    HorizontalLayout buttonLayout = new HorizontalLayout();
    buttonLayout.setSpacing(true);
    buttonLayout.addComponent(exportButton);
    FormLayout formView = new FormLayout();
    uploadReceiver = new UploadReceiver(Path.getTempDirectory(), translator);
    uploadField = new Upload("XLIFF-Datei", uploadReceiver);
    formView.addComponent(uploadField);
    formView.addComponent(buttonLayout);
    root.addSection(formView);
    return root;
  }
}
