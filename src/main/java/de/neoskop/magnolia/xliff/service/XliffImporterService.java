package de.neoskop.magnolia.xliff.service;

import static info.magnolia.repository.RepositoryConstants.WEBSITE;

import info.magnolia.context.MgnlContext;
import java.io.File;
import java.io.IOException;
import javax.inject.Singleton;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * @author Arne Diekmann
 * @since 08.05.17
 */
@Singleton
public class XliffImporterService {

  private static final Logger LOG = LogManager.getLogger(XliffImporterService.class);

  public void importFile(File file) {
    try {
      Session jcrSession = MgnlContext.getJCRSession(WEBSITE);
      Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
      NodeList nodeList = doc.getElementsByTagName("target");

      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        String key = node.getParentNode().getAttributes().getNamedItem("id").getNodeValue();
        String value = node.getTextContent();

        String id = key.split("#")[0];
        String property = key.split("#")[1];

        javax.jcr.Node sourceNode = jcrSession.getNodeByIdentifier(id);
        LOG.info(sourceNode.getProperty(property).getString() + " -> " + value);
        sourceNode.setProperty(property, value);
        jcrSession.save();
      }
    } catch (IOException e) {
      LOG.error("Could not read file", e);
    } catch (SAXException | ParserConfigurationException e) {
      LOG.error("XML-parsing failed", e);
    } catch (RepositoryException e) {
      LOG.error("Changing the source node failed", e);
    }
  }
}
