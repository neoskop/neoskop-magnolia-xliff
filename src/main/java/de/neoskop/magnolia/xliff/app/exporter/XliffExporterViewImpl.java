package de.neoskop.magnolia.xliff.app.exporter;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.v7.ui.HorizontalLayout;
import com.vaadin.v7.ui.TextArea;
import info.magnolia.ui.api.app.AppController;
import info.magnolia.ui.api.app.ChooseDialogCallback;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.form.field.LinkField;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemId;
import info.magnolia.ui.vaadin.integration.jcr.JcrItemUtil;
import info.magnolia.ui.vaadin.layout.SmallAppLayout;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class XliffExporterViewImpl implements XliffExporterView {

  private static final Logger LOG = LogManager.getLogger(XliffExporterViewImpl.class);
  private final SmallAppLayout root = new SmallAppLayout();
  private final AppController appController;
  private final UiContext uiContext;
  private Listener listener;
  private LinkField linkField;

  @Inject
  public XliffExporterViewImpl(AppController appController, UiContext uiContext) {
    this.appController = appController;
    this.uiContext = uiContext;
  }

  public Component asVaadinComponent() {
    linkField = createLinkField();
    final TextArea textArea = createTextArea();

    Button exportButton =
        new Button(
            "Exportieren",
            (Button.ClickListener)
                event -> {
                  final String path = linkField.getValue();
                  final String properties = textArea.getValue();
                  listener.export(path, properties);
                });
    exportButton.addStyleName("v-button-smallapp");
    exportButton.addStyleName("commit");

    HorizontalLayout buttonLayout = new HorizontalLayout();
    buttonLayout.setSpacing(true);
    buttonLayout.addComponent(exportButton);

    FormLayout formView = new FormLayout();
    formView.addComponent(linkField);
    formView.addComponent(textArea);
    formView.addComponent(buttonLayout);

    root.addSection(formView);

    return root;
  }

  private TextArea createTextArea() {
    final TextArea textArea = new TextArea();
    textArea.setWidth("100%");
    textArea.setCaption("Feldnamen");
    textArea.setDescription(
        "Feldnamen, die exportiert werden sollen, entweder durch Kommas oder Zeilenumbrüche getrennt eingeben");
    return textArea;
  }

  private LinkField createLinkField() {
    final LinkField linkField = new LinkField();
    linkField.setCaption("Seitenbaum");
    linkField.setButtonCaptionNew("Seitenbaum auswählen");
    linkField.setButtonCaptionOther("Seitenbaum wechseln");
    linkField.getSelectButton().addClickListener(createClickListener());
    return linkField;
  }

  private Button.ClickListener createClickListener() {
    return (Button.ClickListener)
        event -> {
          ChooseDialogCallback callback = createChooseDialogCallback();
          String value = linkField.getTextField().getValue();
          if (StringUtils.isNotBlank("/")) {
            appController.openChooseDialog("pages", uiContext, "/", value, callback);
          }
        };
  }

  private ChooseDialogCallback createChooseDialogCallback() {
    return new ChooseDialogCallback() {
      @Override
      public void onItemChosen(String actionName, final Object chosenValue) {
        String newValue = null;

        if (chosenValue instanceof JcrItemId) {
          try {
            javax.jcr.Item jcrItem = JcrItemUtil.getJcrItem((JcrItemId) chosenValue);
            if (jcrItem.isNode()) {
              final Node selected = (Node) jcrItem;
              newValue = selected.getPath();
            }
          } catch (RepositoryException e) {
            LOG.error("Not able to access the configured property. Value will not be set.", e);
          }
        } else {
          newValue = String.valueOf(chosenValue);
        }

        linkField.setValue(newValue);
      }

      @Override
      public void onCancel() {}
    };
  }

  @Override
  public void setListener(Listener listener) {
    this.listener = listener;
  }
}
