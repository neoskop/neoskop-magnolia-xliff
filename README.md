# README

Allgemeine Beschreibung vom Modul **Xliff**.

# Abhängigkeiten

Bis Version 1.1.1:

- [Magnolia CMS][1] >= 5.3.7

Ab Version 1.2.0:

- [Magnolia CMS][1] >= 5.6.7

# Installation

Das Modul muss in der `pom.xml` des Magnolia-Projektes als Abhängigkeit hinzugefügt werden:

```xml
<dependency>
	<groupId>org.bitbucket.neoskop</groupId>
	<artifactId>neoskop-magnolia-xliff</artifactId>
	<version>1.2.0</version>
</dependency>
```

Mehr Informationen zur Benutzung

[1]: https://www.magnolia-cms.com
[2]: http://maven.neoskop.io
[3]: http://maven.apache.org/maven-release/maven-release-plugin/
