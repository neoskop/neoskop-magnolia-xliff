package de.neoskop.magnolia.xliff.app.exporter;

import info.magnolia.ui.api.view.View;

/**
 * @author Arne Diekmann
 * @since 03.04.17
 */
public interface XliffExporterView extends View {

  void setListener(Listener listener);

  interface Listener {

    void export(String path, String properties);
  }
}
