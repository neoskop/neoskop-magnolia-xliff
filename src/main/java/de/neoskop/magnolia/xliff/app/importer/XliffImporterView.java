package de.neoskop.magnolia.xliff.app.importer;

import info.magnolia.ui.api.view.View;
import java.io.File;

/**
 * @author Arne Diekmann
 * @since 08.05.17
 */
public interface XliffImporterView extends View {

  void setListener(XliffImporterView.Listener listener);

  interface Listener {

    void importXliff(File file);
  }
}
