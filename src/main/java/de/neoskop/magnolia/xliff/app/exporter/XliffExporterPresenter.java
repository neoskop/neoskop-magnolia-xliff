package de.neoskop.magnolia.xliff.app.exporter;

import static org.apache.commons.lang3.CharEncoding.UTF_8;

import com.vaadin.server.Page;
import de.neoskop.magnolia.xliff.service.XliffExporterService;
import info.magnolia.context.MgnlContext;
import info.magnolia.ui.api.context.UiContext;
import info.magnolia.ui.framework.util.TempFileStreamResource;
import info.magnolia.ui.vaadin.overlay.MessageStyleTypeEnum;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Arne Diekmann
 * @since 03.04.17
 */
public class XliffExporterPresenter implements XliffExporterView.Listener {

  private static final Logger LOG = LogManager.getLogger(XliffExporterPresenter.class);
  private final XliffExporterService xliffExporterService;
  private final XliffExporterView view;
  private final UiContext context;

  @Inject
  public XliffExporterPresenter(
      XliffExporterService xliffExporterService, XliffExporterView view, UiContext context) {
    this.xliffExporterService = xliffExporterService;
    this.view = view;
    this.context = context;
  }

  XliffExporterView start() {
    view.setListener(this);
    return view;
  }

  @Override
  public void export(String path, String properties) {
    final List<String> propertyList = Arrays.asList(properties.split("[\\n,]"));
    final String xliff = xliffExporterService.export(path, propertyList);

    if (StringUtils.isBlank(xliff)) {
      context.openNotification(MessageStyleTypeEnum.WARNING, true, "Got an empty result");
      return;
    }

    final TempFileStreamResource stream = getStreamResource();

    try (final OutputStream out = stream.getTempFileOutputStream()) {
      out.write(xliff.getBytes(UTF_8));
      out.flush();
      Page.getCurrent().open(stream, "", true);
    } catch (IOException e) {
      LOG.error("Could not access temporary file", e);
      context.openNotification(
          MessageStyleTypeEnum.ERROR, true, "Failed to write translations to a file");
    }
  }

  private TempFileStreamResource getStreamResource() {
    final String forwardedHost = MgnlContext.getWebContext().getRequest().getHeader("Host");
    final String filename =
        StringUtils.isBlank(forwardedHost)
            ? "translation.xliff"
            : "translation_" + forwardedHost + ".xliff";
    final TempFileStreamResource stream = new TempFileStreamResource(filename);
    stream.setTempFileName(filename);
    stream.setTempFileExtension("xliff");
    stream.setMIMEType("application/xliff+xml");
    return stream;
  }
}
