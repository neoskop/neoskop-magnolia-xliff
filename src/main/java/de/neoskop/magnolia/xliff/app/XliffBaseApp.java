package de.neoskop.magnolia.xliff.app;

import info.magnolia.ui.api.app.AppContext;
import info.magnolia.ui.api.app.AppView;
import info.magnolia.ui.api.location.DefaultLocation;
import info.magnolia.ui.api.location.Location;
import info.magnolia.ui.framework.app.BaseApp;
import javax.inject.Inject;

public class XliffBaseApp extends BaseApp {

  @Inject
  public XliffBaseApp(AppContext appContext, AppView view) {
    super(appContext, view);
  }

  @Override
  public void start(Location location) {
    super.start(location);
    getAppContext()
        .openSubApp(new DefaultLocation(Location.LOCATION_TYPE_APP, "xliff", "exporter", ""));
    getAppContext()
        .openSubApp(new DefaultLocation(Location.LOCATION_TYPE_APP, "xliff", "importer", ""));
  }
}
