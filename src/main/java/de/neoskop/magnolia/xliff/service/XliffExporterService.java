package de.neoskop.magnolia.xliff.service;

import info.magnolia.context.MgnlContext;
import info.magnolia.jcr.util.NodeUtil;
import java.io.StringWriter;
import java.util.List;
import javax.inject.Singleton;
import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.RepositoryException;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Singleton
public class XliffExporterService {

  private static final Logger LOG = LogManager.getLogger(XliffExporterService.class);

  public String export(String path, List<String> propertyList) {
    StringBuilder xliff = new StringBuilder();

    try {
      Node content = MgnlContext.getJCRSession("website").getNode(path);
      StringWriter sw = new StringWriter();
      writeProperties(content, sw, propertyList);
      visit(content, sw, propertyList);
      xliff.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
      xliff.append("<xliff version=\"1.2\">");
      xliff.append(
          String.format(
              "<file original=\"%s\" source-language=\"de\" datatype=\"html\">",
              content.getPath()));
      xliff.append("<body>");
      xliff.append(sw.toString());
      xliff.append("</body>");
      xliff.append("</file>");
      xliff.append("</xliff>");
      return xliff.toString();
    } catch (RepositoryException e) {
      LOG.error("Failed to access nodes for Xliff export", e);
    }

    return null;
  }

  private void writeProperties(Node node, StringWriter out, List<String> propertyList)
      throws RepositoryException {
    PropertyIterator pi = node.getProperties();
    while (pi.hasNext()) {
      Property p = pi.nextProperty();
      if (propertyList.contains(p.getName())) {
        String name = p.getName();
        String value = StringEscapeUtils.escapeXml(StringEscapeUtils.unescapeHtml(p.getString()));
        if (StringUtils.isNotBlank(value)) {
          out.write(
              String.format(
                  "<trans-unit id=\"%s#%s\">%n<source xml:lang=\"de\">%s</source>%n<target>%s</target>%n</trans-unit>%n%n",
                  node.getIdentifier(), name, value, value));
        }
      }
    }
  }

  private void visit(Node parent, StringWriter out, List<String> propertyList)
      throws RepositoryException {
    for (Node node : NodeUtil.getNodes(parent)) {
      writeProperties(node, out, propertyList);
      visit(node, out, propertyList);
    }
  }
}
